# Based on the file created for Arch Linux by:
# Maintainer : Thomas Baechler <thomas@archlinux.org>

# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Roland Singer <roland@manjaro.org>

_linuxprefix=linux54
_extramodules=extramodules-5.4-MANJARO
pkgname=$_linuxprefix-nvidia-340xx
_pkgname=nvidia
pkgver=340.107
pkgrel=0.8
provides=("$_pkgname=$pkgver")
groups=("$_linuxprefix-extramodules")
pkgdesc="NVIDIA drivers for linux."
arch=('i686' 'x86_64')
url="http://www.nvidia.com/"
depends=("$_linuxprefix" "nvidia-340xx-utils=${pkgver}")
makedepends=("$_linuxprefix-headers")
conflicts=('nvidia-96xx' 'nvidia-183xx' "$_linuxprefix-nvidia" "$_linuxprefix-nvidia-304xx")
license=('custom')
install=nvidia.install
options=(!strip !ccache)
source=('kernel-4.11.patch' 'kernel-5.0.patch' 'kernel-5.2.patch'  'kernel-5.3.patch')
source_i686=("https://us.download.nvidia.com/XFree86/Linux-x86/${pkgver}/NVIDIA-Linux-x86-${pkgver}.run")
source_x86_64=("https://us.download.nvidia.com/XFree86/Linux-x86_64/${pkgver}/NVIDIA-Linux-x86_64-${pkgver}-no-compat32.run")
md5sums=('1e5c198f53bf88b5160ebca8d3f602cb'
         '0ff09b69db7d3da1a56d3455f6587679'
         '164fac79ada52b2a8723ed30b529a624'
         '59b5eadab6df9b070866e239776ca32c')
md5sums_i686=('9a4b382ef545d836033630224735d5dd')
md5sums_x86_64=('ffca1879d77dfc491dfb0276ceb56cc1')

[[ "$CARCH" = "i686" ]] && _pkg="NVIDIA-Linux-x86-${pkgver}"
[[ "$CARCH" = "x86_64" ]] && _pkg="NVIDIA-Linux-x86_64-${pkgver}-no-compat32"

prepare() {
    sh "${_pkg}.run" --extract-only
    cd "${_pkg}"
    # patches here

    # Fix compile problem with 4.11
    (cd kernel; patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-4.11.patch)

    # Fix compile problem with 5.0
    (cd kernel; patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.0.patch)

    (cd kernel; patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.2.patch)

    (cd kernel; patch -p1 --no-backup-if-mismatch -i "$srcdir"/kernel-5.3.patch)

    export DISTCC_DISABLE=1
    export CCACHE_DISABLE=1
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

    cd "${_pkg}"/kernel
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module

    cd uvm
    make SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
    install -D -m644 "${srcdir}/${_pkg}/kernel/nvidia.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia.ko"
    install -D -m644 "${srcdir}/${_pkg}/kernel/uvm/nvidia-uvm.ko" \
        "${pkgdir}/usr/lib/modules/${_extramodules}/nvidia-uvm.ko"
    gzip "${pkgdir}/usr/lib/modules/${_extramodules}/"*.ko
    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
}
